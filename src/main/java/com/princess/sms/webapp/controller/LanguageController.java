package com.princess.sms.webapp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.princess.sms.model.LanguageList;
import com.princess.sms.service.LanguageService;

/**
 * @author <a href="mailto:fvinluan@hagroup.com">Francis Vinluan</a>
 * @version $Revision: 1.0 $
 */
@RestController
public class LanguageController {

  @Autowired
  LanguageService languageService;


  /**
   * Method findSupportedLanguages.
   * 
   * @return LanguageList
   */
  @RequestMapping("/languages/supported")
  public LanguageList findSupportedLanguages() {
    return languageService.findSupportedLanguages();
  }
}
